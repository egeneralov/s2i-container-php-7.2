PHP 7.2 s2i image
================

This container image includes PHP 7.2 as a [S2I](https://github.com/openshift/source-to-image) base image for your PHP 7.2 applications.
The resulting image can be run using [Docker](http://docker.io).

Description
-----------

PHP 7.2 available as container is a base platform for
building and running various PHP 7.2 applications and frameworks.
PHP is an HTML-embedded scripting language. PHP attempts to make it easy for developers 
to write dynamically generated web pages. PHP also offers built-in database integration 
for several commercial and non-commercial database management systems, so writing 
a database-enabled webpage with PHP is fairly simple. The most common use of PHP coding 
is probably as a replacement for CGI scripts.

This container image includes an npm utility, so users can use it to install JavaScript
modules for their web applications. There is no guarantee for any specific npm or nodejs
version, that is included in the image; those versions can be changed anytime and
the nodejs itself is included just to make the npm work.

Usage
---------------------
To build a simple application using standalone [S2I](https://github.com/openshift/source-to-image) and then run the resulting image with [Docker](http://docker.io) execute:

    ```
    $ cd my/cool/php/application/
    $ s2i build . egeneralov/s2i-container-php-7.2 php-test-app
    $ docker run -d --name php-test-app -p 127.0.0.1:8080:8080 php-test-app
    ```

**Accessing the application:**

    ```
    $ curl 127.0.0.1:8080
    ```

Source repository layout
------------------------

You do not need to change anything in your existing PHP project's repository.
However, if these files exist they will affect the behavior of the build process:

* **composer.json**

  List of dependencies to be installed with `composer`. The format is documented
  [here](https://getcomposer.org/doc/04-schema.md).


Extending image
---------------
Not only content, but also startup scripts and configuration of the image can
be extended using [source-to-image](https://github.com/openshift/source-to-image).

The structure of the application can look like this:

| Folder name       | Description                |
|-------------------|----------------------------|
| `./php-pre-start` | Can contain shell scripts (`*.sh`) that are sourced before application is started |
| `./`              | Application source code |


See also
--------

Dockerfile and other sources are available on https://gitlab.com/egeneralov/s2i-container-php-7.2.
