FROM egeneralov/s2i-base-debian

ENV PHP_VERSION=7.2 \
    NAME=php

ENV SUMMARY="Platform for building and running PHP 7.2 applications" \
    DESCRIPTION="PHP 7.2 available as container is a base platform for \
building and running various PHP 7.2 applications and frameworks. \
PHP is an HTML-embedded scripting language. PHP attempts to make it easy for developers \
to write dynamically generated web pages. PHP also offers built-in database integration \
for several commercial and non-commercial database management systems, so writing \
a database-enabled webpage with PHP is fairly simple. The most common use of PHP coding \
is probably as a replacement for CGI scripts."

LABEL summary="${SUMMARY}" \
      description="${DESCRIPTION}" \
      io.k8s.description="${DESCRIPTION}" \
      io.k8s.display-name="PHP ${PHP_VERSION}" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="builder,${NAME},${NAME}${PHP_VERSION}" \
      io.openshift.s2i.scripts-url="image:///usr/libexec/s2i" \
      io.s2i.scripts-url="image:///usr/libexec/s2i" \
      name="egeneralov/s2i-container-${NAME}-${PHP_VERSION}" \
      com.redhat.component="egeneralov/s2i-container-${NAME}-${PHP_VERSION}" \
      version="${PHP_VERSION}" \
      help="hub.docker.com & gitlab.com: egeneralov/s2i-container-${NAME}-${PHP_VERSION}" \
      usage="s2i build . egeneralov/s2i-container-${NAME}-${PHP_VERSION} sample-php-application" \
      maintainer="Eduard Generalov <eduard@generalov.net>"

RUN curl -sL https://packages.sury.org/php/apt.gpg | apt-key add - 2>/dev/null && \
echo "deb https://packages.sury.org/php/ stretch main" > /etc/apt/sources.list.d/php.list && \
apt-get update -q && \
apt-cache search php | grep php7.2 | awk '{print $1}' | grep -Ev 'lib|dbg|sodium' | xargs apt-get install -yq librabbitmq-dev && \
pecl install amqp && \
echo 'extension=amqp.so' | tee /etc/php/7.2/cli/conf.d/20-amqp.ini | tee /etc/php/7.2/fpm/conf.d/20-amqp.ini && \
apt-get autoremove -yq && \
apt-get autoclean -yq && \
apt-get clean -yq


ENV PHP_CONTAINER_SCRIPTS_PATH=/usr/share/container-scripts/php/ \
    APP_DATA=${APP_ROOT}/src

# Copy the S2I scripts from the specific language image to $STI_SCRIPTS_PATH
COPY ./s2i/bin/ $STI_SCRIPTS_PATH

# Copy extra files to the image.
COPY ./root/ /

# Reset permissions of filesystem to default values
# RUN /usr/libexec/container-setup && rpm-file-permissions

USER 1001

# Set the default CMD to print the usage of the language image
CMD $STI_SCRIPTS_PATH/usage
